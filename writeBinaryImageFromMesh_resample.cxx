#include "itkMesh.h"
#include "itkMeshFileReader.h"

#include "itkImage.h"
#include "itkImageFileReader.h"
#include "itkImageFileWriter.h"

#include "itkTranslationTransform.h"
#include "itkResampleImageFilter.h"

#include "itkCastImageFilter.h"
#include "itkTriangleMeshToBinaryImageFilter.h"

int main( int argc, char* argv[] )
{
  if( argc != 5 )
    {
    std::cerr << "Usage: "<< std::endl;
    std::cerr << argv[0];
    std::cerr << " <InputImageName> <TranslatedImageName> <InputMeshName> <OutputImageName>";
    std::cerr << std::endl;
    return EXIT_FAILURE;
    }

  const char * inputImageName   = argv[1];
  const char * transImageName   = argv[2];
  const char * inputMeshName    = argv[3];
  const char * outputImageName  = argv[4];

  const unsigned int Dimension = 3;
  typedef double MeshPixelType;

  typedef itk::Mesh< MeshPixelType, Dimension > MeshType;

  typedef itk::MeshFileReader< MeshType >  MeshReaderType;
  MeshReaderType::Pointer meshReader = MeshReaderType::New();
  meshReader->SetFileName( inputMeshName );

  typedef unsigned char                           InputPixelType;
  typedef itk::Image< InputPixelType, Dimension > InputImageType;
  typedef itk::ImageFileReader< InputImageType >  ImageReaderType;
  typedef itk::ImageFileWriter< InputImageType >  ImageWriterType;

  typedef unsigned char                             OutputPixelType;
  typedef itk::Image< OutputPixelType, Dimension >  OutputImageType;

  ImageReaderType::Pointer imageReader = ImageReaderType::New();
  ImageWriterType::Pointer imageWriter = ImageWriterType::New();
  imageReader->SetFileName( inputImageName );
  imageWriter->SetFileName( transImageName );

  InputImageType::ConstPointer inputImage = imageReader->GetOutput(); 
  InputImageType::PointType     origin    = inputImage->GetOrigin();
  InputImageType::SpacingType   spacing   = inputImage->GetSpacing();
  InputImageType::DirectionType direction = inputImage->GetDirection();
  InputImageType::RegionType    region    = inputImage->GetLargestPossibleRegion();

  double translationX = 5.0;
  double translationY = -40.0;
  double translationZ = -200.0;

//  InputImageType::PointType::VectorType translation1;
//  translation1[0] = translationX;
//  translation1[1] = translationY;
//  translation1[2] = translationZ;

//  origin += translation1;

  typedef itk::TranslationTransform< double, Dimension > TransformType;
  TransformType::Pointer transform = TransformType::New();
  TransformType::OutputVectorType translation;
  translation[0] = translationX;
  translation[1] = translationY;
  translation[2] = translationZ;
  transform->Translate(translation);

  typedef itk::ResampleImageFilter< InputImageType, InputImageType > ResampleFilterType;
  ResampleFilterType::Pointer resampler = ResampleFilterType::New();
  resampler->SetInput( imageReader->GetOutput() );
  resampler->SetTransform( transform.GetPointer() );

  resampler->SetSize( inputImage->GetSize() );
//  resampler->SetOutputOrigin(  origin );
//  resampler->SetOutputSpacing( spacing );
//  resampler->SetOutputDirection( direction );
//  resampler->SetDefaultPixelValue( 0 );
  resampler->Update();

  imageWriter->SetInput( resampler->GetOutput() );
  try
    {
    imageWriter->Update();
    }
  catch( itk::ExceptionObject & excp )
    {
    std::cerr << excp << std::endl;
    return EXIT_FAILURE;
    }


  typedef itk::CastImageFilter< InputImageType, OutputImageType > CastFilterType;
  CastFilterType::Pointer cast = CastFilterType::New();
  cast->SetInput( resampler->GetOutput() );

  typedef itk::TriangleMeshToBinaryImageFilter< MeshType, OutputImageType > FilterType;
  FilterType::Pointer filter = FilterType::New();
  filter->SetInput( meshReader->GetOutput() );
  filter->SetInfoImage( cast->GetOutput() );
  filter->SetInsideValue( itk::NumericTraits< OutputPixelType >::max() );
  try
    {
    filter->Update();
    }
  catch( itk::ExceptionObject & error )
    {
    std::cerr << "Error: " << error << std::endl;
    return EXIT_FAILURE;
    }

  typedef itk::ImageFileWriter< OutputImageType > WriterType;
  WriterType::Pointer writer = WriterType::New();
  writer->SetFileName( outputImageName );
  writer->SetInput( filter->GetOutput() );
  try
    {
    writer->Update();
    }
  catch( itk::ExceptionObject & error )
    {
    std::cerr << "Error: " << error << std::endl;
    return EXIT_FAILURE;
    }

  std::cout << "**************************************" << std::endl;

  return EXIT_SUCCESS;
}

